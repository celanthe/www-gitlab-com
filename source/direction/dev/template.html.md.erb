---
layout: markdown_page
title: "Product Section Vision - Dev"
---

- TOC
{:toc}

![Dev Overview](/images/direction/dev/dev-overview.png)

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/mIpHEbyhsj0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Dev Section Overview

The Dev Section is made up of the [Manage](/handbook/product/categories/#manage-stage), [Plan](/handbook/product/categories/#plan-stage), and [Create](/handbook/product/categories/#create-stage) stages of the DevOps lifecycle. These stages mark the leftmost side of the DevOps lifecycle and primarily focus on the creation and development of software. The scope for Dev stages is wide and encompasses a number of analyst categories including Value Stream Management, Project Portfolio Management, Enterprise Agile Planning Tools, Source Code Management, IDEs, Design Management, and even ITSM. It is difficult to truly estimate TAM for the Dev Section, as our scope includes so many components from various industries, but research indicates the estimated [TAM](https://docs.google.com/spreadsheets/d/1HYi_l8v-wTE5-BUq_U29mm5aWNxnqjv5vltXdT4XllU/edit?usp=sharing) in 2019 is roughly ~$3B, growing to ~$7.5B in 2023 (26.5% CAGR).

Based on [DevOps tool revenue](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) at the end of 2018 and comparing to GitLab annual recurring revenue at the end of FY20 Q3, our estimated market share is approximately 1.5% based on revenue. (Note: this assumes we can attribute 100% of GitLab revenue to Dev stages.) Market share based on source code management is somewhere in the [30%](https://docs.google.com/document/d/15TLEUc9BxiiB9N33MW-7zGvfitfFXQj3TM8BfM2q4hM/edit?usp=sharing) range.

Nearly [half of organizations](https://drive.google.com/file/d/17ZSI2hGg3RK168KHktFOiyyzRA93uMbd/view?usp=sharing) still have not adopted DevOps methodologies, despite [data](https://drive.google.com/file/d/17MNecg84AepxWlSDB5HjNBrCJggaS9tP/view?usp=sharing) that indicates far higher revenue growth for organizations that do so. Migrating a code base to a modern, Git-backed source control platform like GitLab can often be the first step in a DevOps transformation. As such, we must provide industry-leading solutions in source code and code review, as this is not only the entry into DevOps for our customers, but typically the entry into the GitLab platform. Once a user has begun utilizing repositories and code review features like Merge Requests, they often move “left” and “right” to explore and utilize other capabilities in GitLab, such as CI and project management features.

Per our [Stage Monthly Active User Data](https://app.periscopedata.com/app/gitlab/425984/Month-over-Month-Overestimated-SMAU), Manage and Create have the highest usage amongst GitLab stages. As such, these stages must focus on security fixes, bug fixes, performance improvements, UX improvements, and depth more than other areas of GitLab. Plan, while introduced in 2011 with the release of issue tracking, still falls far behind market leaders who have better experiences for sprint management, portfolio management, roadmapping, and workflows.

Other areas, such as Value Stream Management are nascent to both GitLab and the market, and will require more time devoted to executing problem and solution validation discovery. 

Over the next year, Dev will require focus on both breadth and depth activities, and each stage will require significant investment to accelerate the delivery of security issues, performance issues, and direction items. 

## Resourcing and Investment

The existing team members for the Dev section can be found in the links below:
* [Development](/company/team/?department=dev-section)
* [User Experience](/company/team/?department=dev-ux-team)
* [Product Management](/company/team/?department=dev-pm-team)
* [Quality Engineering](/company/team/?department=dev-qe-team)

## Dev Section SWOT Analysis & Challenges

### Strengths

* Complete DevOps platform with seamless connections to downstream sections like CI/CD, Ops, Secure, and Defend.
* Industry leading source code management and code review products.
* Ability to start with GitLab just on the use case needed.
* Strong analyst relationships allow us to help define nascent markets like Value Stream Management.
* Exceptionally talented product team with deep industry expertise. 
* [Open core](/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) with both self-managed and SaaS options.
* Expansive feature set and roadmap that aligns to validated market needs. 
* We’ve chosen to invest in a storage architecture ([Gitaly](/blog/2018/09/12/the-road-to-gitaly-1-0/)) that will lead to future performance enhancements and high-availability options.
* Investment into policy compliance and auditability, enabling enterprise use cases.
* Dev has high adoption rate and high usage, enabling us to introduce new features and verify direction early on.

### Weaknesses

* Our SaaS product (GitLab.com) is not yet enterprise grade, and several improvements are needed for enterprise adoption of self-managed instances.
* Performance issues continue to mount in various areas (ex: MR load times, diff rendering, Web IDE load times) as we add new features and code.
* Some areas of Dev are not competitive; for example Wikis, Snippets, and Roadmaps.
* Our company name may be limiting our opportunities for the Plan stage by creating the sense that the features only work for the software development lifecycle. We may need to consider changing how we package capabilities for customer segments who desire to use GitLab for project or portfolio management.
* We have not put as much effort into use cases or cross-stage features, leading to a poorer than desired user experience.

### Opportunities

* Consideration of new business models/licensing plans could lead to additional adoption of features.
* Take a leadership position in Value Stream Management, a nascent market with many vendors who aren’t doing it well.
* Big opportunity to grow and/or shift usage to SaaS, allowing customers to receive value more quickly from GitLab without provisioning and maintaining their own instance.
* Disrupting dominant legacy designs by creating a modern, lovable system for requirements management, workflows, and permissions.
* Creating an “easy migration” path from other popular DevOps tools like Jenkins and GitHub would expedite our market share growth.
* Increasing investment into Wikis, Gitaly, Compliance, and Boards will provide more focus to these areas, resulting in additional IACV.
* Capitalize on solutions that can tie together information, such as the MR displaying monitoring results, end-to-end tests, etc. and the Web IDE showng timings and stats on functions from actual traces.
* Focus on internal adoption of the features we are shipping. We have a large development team and we should aspire to have them dogfood new features as they are released.

### Threats

* Onboarding new team members may lead to a slow down in velocity if not managed carefully.
* Other companies, such as Microsoft, may begin to innovate more quickly than they have in previous years, closing the cracked window of opportunity for us to gain market share.
* If we stop talking to customers to better understand their problems and pain points, we will continue to ship code and features at a high velocity, but the things we ship may not add value for our customers. This is known as the [build trap](https://melissaperri.com/blog/2014/08/05/the-build-trap).
* Dev tools start to become commoditized, with IaaS providers giving them away for free with IaaS/cloud services contracts.
* The Dev section currently has the highest volume of high-priority defects and security issues, which will naturally impact the ability to ship direction items as quickly as expected. This could result in slower business growth, as direction items are typically tied to IACV or customer upsell/retention opportunities.

## Vision Themes

Our vision for the Dev section is to provide the world’s best product creation platform. We believe we have a massive opportunity to change how cross-functional, multi-level teams collaborate by providng an experience that breaks down organizational silos and enables better collaboration. We want to deliver a solution that enables higher-quality products to be created faster. The following themes are listed below to surface our view of what will be important to the market and to GitLab over the next 3 to 5 years. As such, they will be the cornerstone of our 3-year strategy, and all activities in the 1-year plan should advance GitLab in one or more of these areas.

<%= partial("direction/dev/themes/automated_code_review") %>

<%= partial("direction/dev/themes/value_stream_measurement") %>

<%= partial("direction/dev/themes/more_devops_personas") %>

<%= partial("direction/dev/themes/enterprise_digital_transformation") %>

<%= partial("direction/dev/themes/project_to_product") %>

## 3-Year Strategy

In three years, the Dev Section market will:

* Centralize around Git as the version control of choice for not only code, but for design assets, gaming, silicon designs, and AI/ML models.
* Have a market leader emerge in the value stream management space. Currently, the market is fragmented with most players focused on integrations into various DevOps tools.
* Adopt a mindset shift from project management to product management.
* Recognize the value of a single platform for all software creation activities, including product management.
* See an uptick in startups and applications being built on the backs of a "no code" framework

As a result, in three years, GitLab will:

* Provide a next-generation, highly performant Git-backed version control system for large assets, such as ML models. Our goal in three years should be to host the most repositories of these non-code assets.
* Emerge as the leader in VSM and be recognized in the industry by customers and analysts as such. Our goal in three years should be to provide the best insights into the product development process that no other tool can come close to, as we have a [unified data model](https://www.ca.com/en/blog-itom/what-is-a-unified-data-model-and-why-would-you-use-it.html) due to GitLab being a single platform.
* Develop an industry-leading product management platform where multiple features and products can be measured and managed easily.
* Research and potentially add capabilities for "no code" workflows

## 1-Year Plan: What’s Next for Dev

Over the next 12 months, each stage in the Dev section will play an integral part in this strategy.

Please see the [categories page](/handbook/product/categories/#dev-section) for a more detailed look at Dev's plan by exploring `Strategy` links in areas of interest.

<%= partial("direction/dev/plans/manage") %>

<%= partial("direction/dev/plans/plan") %>

<%= partial("direction/dev/plans/create") %>

### Themes that cross all Dev stages
**Performance and availability:** We must invest in the performance, stability, and availability of our application. We will do this by focusing on application limits and load times and ensuring availability is top of mind.

Growth driver: Retention

### What we're not doing

Choosing to invest in these areas in 2020 means we will choose not to:
* Invest in features that help companies answer, “Am I doing the right activities?” Answering this question is something we will focus on in years two and three of the VSM plan.
* Treat ML models as first-class citizens in GitLab. Instead, we will focus on getting large assets to become performant via improvements to Gitaly. Once this is completed, we will focus on ML models.
* Provide recommendations where customers can improve their efficiency in the DevOps lifecycle. This will likely require comparisons amongst many GitLab users and an AI engine to make intelligent recommendations. These improvements will come in years two and three of the VSM plan.

### Other areas of investment consideration
* Data science: We should consider investment into a data science team that can assist with recommendations for Plan and VSM features.
* [Dark themes](https://gitlab.com/gitlab-org/gitlab-ee/issues/14531): We should consider prioritizing a dark theme for both GitLab, as well as the Web IDE/editing experience. This is an expected feature of most modern development tools.
* Engineering: Most Dev groups should see 50-100% headcount growth in order to make our Dev categories lovable.
* AI: We should consider beginning to invest into AI as a solution for recommendations; for example, recommended assignees, labels, etc.

## Stages & Categories

<%= partial("direction/dev/strategies/manage") %>

<%= partial("direction/dev/strategies/plan") %>

<%= partial("direction/dev/strategies/create") %>


## What's Next 

<%= direction["all"]["all"] %>
