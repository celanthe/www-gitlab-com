---
layout: markdown_page
title: "Creator pairing"
---

## Introduction

Creator pairing is a program that allows any GitLab engineer to work together with GitLab co-founder Dmitriy Zaporozhets (DZ) from the same physical location for a week.

## Goal

The goal of the program is to help DZ during his work as a fellow of initial delight.
As an additional benefit, a co-worker will learn about how DZ thinks and sees the product.

## Things to Know

* You need to travel to DZ location in Kharkiv, Ukraine or Limassol, Cyprus.
* The duration is one week per person.
* Don't plan to do any of your usual work. Tell your team you're on paid time off.
* The company will pay for travel and hotel.

## Who can apply

Any [senior or higher](/handbook/engineering/career-development/#engineering) backend or frontend engineer at GitLab.

## How to apply

1. Create a merge request to add yourself to the [schedule](#schedule).
1. Ask your manager to approve (but not merge) the merge request.
1. Assign the merge request to DZ (`@dzaporozhets`).

## Schedule

| Start date | End date | Who | Location |
|---|---|---|--- |
| 2019-11-04 | 2019-11-08 | [Tetiana Chupryna ](https://gitlab.com/brytannia) | Kharkiv, Ukraine |
| 2019-12-09 | 2019-12-13 | [Sam Beckham](https://gitlab.com/samdbeckham) | Limassol, Cyprus |
| 2019-12-30 | 2020-01-10 | `NOT AVAILABLE` | N/A |
| 2020-01-27 | 2020-01-31 | [Imre Farkas](https://gitlab.com/ifarkas) | Kharkiv, Ukraine |
| 2020-02-03 | 2020-02-07 | [Jason Plum](https://gitlab.com/warheadsse) | Kharkiv, Ukraine |
| 2020-02-10 | 2020-02-14 | [Douglas Barbosa Alexandre](https://gitlab.com/dbalexandre) | Kharkiv, Ukraine |

## FAQ

- Q: Is a visa to travel into Ukraine?

  A: [Ministry of Foreign Affairs of Ukraine](https://mfa.gov.ua/en/consular-affairs/entering-ukraine/visa-requirements-for-foreigners) can advise if a visa is necessary based on your country of origin.

- Q: Who books travel? 

  A: In most cases the participant will book their flight to Ukraine or Cyprus, but the EBA to Engineering is available to assist.
  
- Q: What airport should participants fly in to?

  A: Once manager approval has been received and MR has been merged, DZ will add the location in which the rotation will occur.  Post in [#creator-pairing](https://gitlab.slack.com/messages/CPD077F7V) channel on Slack and DZ will advise of the ideal airports to travel into for the upcoming rotation.  *This can vary for each rotation* even if the pairing is taking place in the same city.
  
- Q: Who will book hotel or accommodations for the rotation?  
  
  A: [EBA to Engineering](/handbook/eba/#executive-business-administrator-team) can book a hotel or AirBnB on behalf of the participants.  If a participant would like to book their own accommodations this is perfectly acceptable, please communicate in the #creator-pairing Slack channel to advise.
  
  
  
  
  
  
  
