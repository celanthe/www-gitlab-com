---
layout: handbook-page-toc
title: Live Learning
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Live Learning Schedule

1. The 2019 Live Learning schedule is as follows: 
   - October - Communicating Effectively Through Text
   - December - Tentative: Coaching
1. The 2020 Live Learning schedule is as follows: 
   - January - TBC
   - February - TBC
   - March - TBC
   - April - TBC
   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC
