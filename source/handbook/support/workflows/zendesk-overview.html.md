---
layout: handbook-page-toc
title: Zendesk Overview
category: Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Zendesk Overview

## Apps

- Public Apps

    - [Unbabel for Zendesk](https://about.gitlab.com/handbook/support/workflows/unbabel_translation_in_zendesk.html)
    - Salesforce
    - Advanced Search
    - Show Related Ticket
    - Slack App
    - Tag Locker
- Private Apps

    - 2FA Check
    - Architecture Diagrams 
    - Email Supressions
    - Add Issue Comment

## Business Rules

- Triggers
- Automations
- Service Level Agreements

## Emails

## Organizations

## Macros 

## Reporting

## Support Operations
- [Customer Satisfaction Survey(CSAT)](https://about.gitlab.com/handbook/support/support-ops/#customer-satisfaction-survey-csat)
- [Shared Organizations in Zendesk](/handbook/support/support-ops/#shared-organizations-in-zendesk)
- [Salesforce - Zendesk sync](/handbook/support/support-ops/#salesforce---zendesk-sync)
- [PagerDuty](/handbook/support/support-ops/#pagerduty)
- [Slack Notifications](https://about.gitlab.com/handbook/support/support-ops/#slack-notifications)

## Ticket Fields

## Ticket Forms

## Views

## Zendesk Instances

## Zendesk Administrator 





