---
layout: job_family_page
title: "People Operations Fullstack Engineer"
---
People Operations Fullstack Engineer will work on automating various workflows of People Operation teams, including working on GitLab backend (Ruby on Rails), and frontend (Vue.js).
Due to the nature of the work and projects involved in People Operations, this role will require automating manual or semi-automated workflows across various other platforms used by the People Operations teams. This position reports to the Manager, People Operations.


<a id="intermediate-requirements"></a>
### Responsibilities

- Collaborate with all team members in PeopleOps, as well as Engineering and Product to work on automation between various systems and creating smaller improvements inside of GitLab. 
- Advocate for improvements to people operations projects, issue tracker and project in GitLab-com.
- Improve and automate our Off-boarding process, to ensure a more rapid and voluntary workflow.
- Improving our Data Analytics dashboards. 
- Automation of various mundane tasks.
- API building for external systems (payroll, benefits, etc). 
- Involvement in Compensation Calculator Design/updates.
- Architect and implement PeopleOps support tool to streamline questions from multiple sources.
- Auditing People Ops processes/iterating for efficiency.
- Automate updates to the about.gitlab.com/jobs page
- Update the general jobs page.
- Improvement and automation of contract creation
- Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
- Represent GitLab and its values in public communication around specific projects and community contributions.


### Requirements

- Sincere interest in working on People Operations or Operations related Engineering tasks.
- Professional experience with Ruby and Rails.
- Professional experience with JavaScript and associated web technologies (CSS, semantic HTML).
- Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment.
- Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions.
- Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems.
- Comfort working in a highly agile, intensely iterative software development process.
- Demonstrated ability to onboard and integrate with an organization long-term.
- Positive and solution-oriented mindset.
- Effective communication skills: Regularly achieve consensus with peers, and clear status updates.
- An inclination towards communication, inclusion, and visibility.
- Experience owning a project from concept to production, including proposal, discussion, and execution.
- Self-motivated and self-managing, with strong organizational skills.
- Demonstrated ability to work closely with other parts of the organization.
- Share our values, and work in accordance with those values.
- Ability to thrive in a fully remote organization.

### Nice-to-haves

- Experience working with modern Frontend frameworks (eg. React, Vue.js, Angular).
- Experience in a peak performance organization, preferably a tech startup.
- Experience with the GitLab product as a user or contributor.
- Experience working with a remote team.
- Enterprise software company experience.
- Experience working with a global or otherwise multicultural team.
- Passionate about/experienced with open source and developer tools and how we use it in non-engineering teams.

### Performance Indicators 
- [Ship X% of work scope within agreed timeframe](/handbook/people-group/people-group-metrics/#ship-x-of-work-scope-within-agreed-timeframe)
- [Proposals defined for each issue](/handbook/people-group/people-group-metrics/#proposals-defined-for-each-issue--100)
